﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    //The parent class
    public abstract class Command
    {
        public abstract void Execute();
        public abstract void UnExecute();


    }

    
